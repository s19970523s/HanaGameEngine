package com.crater.hanaBot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HanaBotApplication {

	public static void main(String[] args) {
		SpringApplication.run(HanaBotApplication.class, args);
	}

}
