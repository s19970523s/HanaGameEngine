package com.crater.hanaBot.controller;

import org.apache.commons.logging.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 這邊主要負責api的接口，不做任何的處理直接轉去controller
 */
@RestController
public class RestfulController {

    /**
     * 用於發訊息
     * @param content
     * @return 回傳你傳送的訊息內容
     */
    @GetMapping("/chat/{content}")
    public String chat(@PathVariable("content") String content) {
        return content;
    }

//    @PostMapping //TODO: 新版的spring好像可以自動解析json轉物件，研究一下用的
}
