package com.crater.hanaBot.service;

import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

@Component
public class GameScriptService {
    public String readLine(int lineNumber, String filePath) throws FileNotFoundException {
        String readLine = new String();
        try {
            FileReader fileReader = new FileReader(filePath);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            readLine = bufferedReader.readLine();
//            System.out.println(bufferedReader.readLine());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw e;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return readLine;
    }
}
